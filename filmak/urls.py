from django.conf.urls import patterns, include, url
from django.views.generic import DetailView, ListView
from filmak.models import Filma, Bozkatzailea
from django.contrib.auth import views as auth_views

urlpatterns = patterns('filmak.views',
    url(r'^$','index',name='home'),
    url(r'^logout$','logout_bista'),
    url(r'^logout2$',auth_views.logout,{'next_page':'/'},name='logout'),
    url(r'^signup$','signup_manual_bista'),
    url(r'^signup2$','signup_auto_bista'),
    url(r'^login$','login_manual_bista'),
    url(r'^login2$',auth_views.login,{'template_name': 'filmak/login2.html'}, name='login'),

    # Filmen gunea
    url(r'^films/$','film_taula'),
    url(r'^films/(?P<film_id>\d+)/$','film_bozkatzaileak'),
    url(r'^films/(?P<film_id>\d+)/vote/$','film_bozkatu'),

)

