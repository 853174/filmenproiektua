from filmak.models import Filma, Bozkatzailea
from django.contrib import admin

class FilmaAdmin(admin.ModelAdmin):
	filedsets = [
		('Filmaren informazioa',{'fields':['izenburua','zuzendaria','urtea','generoa','sinopsia']}),
		(None,			{'fields':['bozkak']}),
	]

class BozkatzaileAdmin(admin.ModelAdmin):
	fieldsets = [
		(None,{'fields':['erabiltzailea']}),
		('Bozkatze zerrenda',{'fields':['gogoko_filmak']})
	]

admin.site.register(Filma, FilmaAdmin)
admin.site.register(Bozkatzailea, BozkatzaileAdmin)
