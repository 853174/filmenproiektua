from django.shortcuts import render_to_response, get_object_or_404
from filmak.models import Filma, Bozkatzailea
from django.http import HttpResponse, HttpResponseRedirect
from django.template import *
from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def index(request):
	return render_to_response('filmak/index.html',
					{'erabiltzailea' : request.user},context_instance=RequestContext(request))

def logout_bista(request):
	logout(request)
	return HttpResponseRedirect('/filmak/')

def signup_auto_bista(request):
	if request.method == 'POST':
		form = UserCreationForm(request.POST)
		if form.is_valid():
			user = form.save()
			return render_to_response('filmak/index.html',{'msg':'Erregistro zuzena, login egin konfirmatzeko'},context_instance=RequestContext(request))
		else:
			form = UserCreationForm()
			return render_to_response('filmak/signup2.html',{'form': form}, context_instance=RequestContext(request))
	else:
		if request.user.is_authenticated():
			return HttpResponseRedirect('/filmak/')
		else:
			form = UserCreationForm()
			return render_to_response('filmak/signup2.html',{'form': form}, context_instance=RequestContext(request))

def signup_manual_bista(request):
	if request.method == 'POST':
		erab = request.POST['erab_izen']
		if erab == "":
			return render_to_response('filmak/signup.html',{'err':'Erabiltzaile izena ezin da hutsa izan!'}, context_instance=RequestContext(request))
		if User.objects.filter(username=erab).exists() :
			return render_to_response('filmak/signup.html',{'err':'Erabiltzailea existitzen da jada. Hautatu beste izen bat'}, context_instance=RequestContext(request))
		
		pass1 = request.POST['erab_pass1']
		pass2 = request.POST['erab_pass2']
		if pass1 == pass2 and pass1 != "":
			# Zuzen
			user = User(username=erab)
			user.save()
			user.set_password(pass1)
			user.save()
			return render_to_response('filmak/index.html',{'msg':'Erregistro zuzena, login egin konfirmatzeko.'},context_instance=RequestContext(request))
		else:	
			# pasahitz okerrak!
			return render_to_response('filmak/signup.html',{'err':'Pasahitz okerra'})
	else:
		if request.user.is_authenticated():
			return HttpResponseRedirect('/filmak/')
		else:
			return render_to_response('filmak/signup.html',context_instance=RequestContext(request))

# Ez da beharrezkoa
#
# def login_auto_bista(request):
#        if request.user.is_authenticated():
#                return HttpResponseRedirect('/filmak/films/')
#        else:
#		return render_to_response('filmak/login2.html',context_instance=RequestContext(request))


def login_manual_bista(request):
        if request.method == 'POST':
		erab = request.POST['erab_izen']
		passwd = request.POST['erab_pass']
		user = authenticate(username=erab, password = passwd)
		if user is not None:
			if user.is_active:
				login(request, user)
				return HttpResponseRedirect('/filmak/films/')
			else:
				return render_to_response('filmak/login.html',{'err':'Kontua desgaituta'},context_instance=RequestContext(request))
		else:
			return render_to_response('filmak/login.html',{'err':'Login desegokia'},context_instance=RequestContext(request))
	else:
		if request.user.is_authenticated():
        	        return HttpResponseRedirect('/filmak/')
        	else:
        	        return render_to_response('filmak/login.html',context_instance=RequestContext(request))


# Filmen gunea

@login_required(login_url='/filmak/login')
def film_taula(request):
	flist = Filma.objects.all().order_by('urtea')
	paginator = Paginator(flist, 4) # Erakutsi 4 filma orrialdeko
	
	page = request.GET.get('page')
	try:
		films = paginator.page(page)
	except PageNotAnInteger:
		films = paginator.page(1)
	except EmptyPage:
		films = paginator.page(paginator.num_pages)

	return render_to_response('filmak/filmak.html',{'film_list':films,'erabiltzailea':request.user})

@login_required(login_url='/filmak/login')
def film_bozkatzaileak(request, film_id):
        f = get_object_or_404(Filma, pk=film_id)
	# Lortu filmaren bozkatzaile guztiak
	bozkatzaile_list = []
	for b in Bozkatzailea.objects.all():
		if b.hasVoted(f.id):
			bozkatzaile_list.append(b)

        return render_to_response('filmak/detail.html',{'film':f,'bozkatzaile_list':bozkatzaile_list,'erabiltzailea':request.user})

@login_required(login_url='/filmak/login')
def film_bozkatu(request, film_id):
        f = get_object_or_404(Filma, pk=film_id)

	# Bozkatzailea eguneratu
	if Bozkatzailea.objects.filter(erabiltzailea=request.user).exists():
		# Bozkatzailea sortuta dago

		b = Bozkatzailea.objects.get(erabiltzailea = request.user)

		# Bozka bat gehitu, jada bozkatu ez badu
	        if not b.hasVoted(f.id):
        	        f.bozkak += 1
        	        f.save()
        	        msg = "Zure botoa zuzen gorde da."
        	else:
        	        msg = "Jada bozkatu duzu filma honetan."

		b.gogoko_filmak.add(f)
		b.save()
		
	else:
		# Bozkatzailea oraindik ez dago sortuta
		b = Bozkatzailea(erabiltzailea = request.user)
		b.save()
		b.gogoko_filmak = []
		b.gogoko_filmak.add(f)
		b.save()

		f.bozkak += 1
		f.save()
		msg = "Zure botoa zuzen gorde da."
        
	return render_to_response('filmak/vote.html',{'msg':msg,'erabiltzailea':request.user,'film':f})
	# return HttpResponseRedirect(reverse('filmak/vote.html', args=(film_id,)))


