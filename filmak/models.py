from django.db import models
from django.contrib.auth.models import User

GENERO_AUKERA = (
        ('TH','Thrilerra'),
        ('DR','Drama'),
        ('ER','Erromantikoa'),
        ('MI','Misterioa'),
        ('KO','Komedia'),
        ('DO','Dokumentala'),
	('SF','Zientzia-fikzioa'),
)

class Filma(models.Model):
        izenburua = models.CharField(max_length=100)
        zuzendaria = models.CharField(max_length=60)
        urtea = models.IntegerField()
        generoa = models.CharField(max_length=2, choices = GENERO_AUKERA)
        sinopsia = models.CharField(max_length=500)
        bozkak = models.IntegerField()

	def __unicode__(self):
		return self.izenburua

class Bozkatzailea(models.Model):
        gogoko_filmak = models.ManyToManyField(Filma)
	erabiltzailea = models.OneToOneField(User)

	def __unicode__(self):
		return self.erabiltzailea.username

	def hasVoted(self,film_id):
		for f in self.gogoko_filmak.all():
			if f.id == film_id:
				return True
		return False

	def getFilmList(self):
		self.gogoko_filmak.all()
			
