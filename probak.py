from filmak.models import Bozkatzailea, Filma
from django.contrib.auth.models import User

# Filmen probak
flist = Filma.objects.all()
flist

f0 = flist[0]
f0.izenburua
f0.bozkak

# Filma berria sortu

fNew = Filma(izenburua='Okja',zuzendaria='Bong Joon-ho',urtea=2017,generoa='SF',sinopsia='A young girl risks everything to prevent a powerful, multinational company from kidnapping her best friend - a fascinating beast named Okja.',bozkak=0)
fNew.save()

flist

# Erabiltzaile berria sortu

me = User(username="proba_user")
me.save()

me.set_password("proba_user")
me.save()

me

# Bozkatzaile berria sortu

bzk = Bozkatzailea(erabiltzailea=me)
bzk.save()

bzk.gogoko_filmak.all()

# Bozkatu

bzk.hasVoted(fNew.id)

bzk.gogoko_filmak.add(fNew)
bzk.save()

fNew.bozkak += 1
fNew.save()

# Egiaztatu emaitzak

bzk.gogoko_filmak.all()
bzk.hasVoted(fNew.id)
fNew.bozkak

